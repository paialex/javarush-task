package com.crudapp.services;

import com.crudapp.dao.UserDAO;
import com.crudapp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Alex on 27-Aug-16.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    public List<User> getAllUsers() {
        return userDAO.getAllUsers();
    }

    public User getUserById(int userId) {
        User user = userDAO.getUserById(userId);
        return user;
    }

    public synchronized boolean addUser(User user) {

        if (userDAO.userExists(user.getUserFirstName(), user.getUserLastName())) {
            return false;
        } else {
            userDAO.addUser(user);
            return true;
        }

    }

    public void updateUser(User user) {
        userDAO.updateUser(user);
    }

    public void deleteUser(int userId) {
        userDAO.deleteUser(userId);
    }
}
