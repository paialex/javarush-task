package com.crudapp.services;

import com.crudapp.model.User;

import java.util.List;

/**
 * Created by Alex on 27-Aug-16.
 */
public interface UserService {
    List<User> getAllUsers();

    User getUserById(int userId);

    boolean addUser(User user);

    void updateUser(User user);

    void deleteUser(int userId);
}
