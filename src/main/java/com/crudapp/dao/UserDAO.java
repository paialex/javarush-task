package com.crudapp.dao;

import com.crudapp.model.User;

import java.util.List;

/**
 * Created by Alex on 27-Aug-16.
 */
public interface UserDAO {
    List<User> getAllUsers();

    User getUserById(int userId);

    boolean addUser(User user);

    void updateUser(User user);

    void deleteUser(int userId);

    boolean userExists(String firstName, String lastName);
}
