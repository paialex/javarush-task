package com.crudapp.dao;

import com.crudapp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Alex on 27-Aug-16.
 */
@Transactional
@Repository
public class UserDAOImpl implements UserDAO {
    @Autowired
    private HibernateTemplate hibernateTemplate;


    @SuppressWarnings("unchecked")
    public List<User> getAllUsers() {

        String hql = "FROM com.crudapp.model.User";
        return (List<User>) hibernateTemplate.find(hql);
    }

    public User getUserById(int userId) {
        return hibernateTemplate.get(User.class, userId);
    }

    public boolean addUser(User user) {
        hibernateTemplate.save(user);
        return false;
    }

    public void updateUser(User user) {
        User u = getUserById(user.getUserId());
        u.setUserFirstName(user.getUserFirstName());
        u.setUserLastName(user.getUserLastName());
        u.setEmail(user.getEmail());
        u.setAge(user.getAge());
        hibernateTemplate.update(u);
    }

    public void deleteUser(int uid) {
        hibernateTemplate.delete(getUserById(uid));
    }

    public boolean userExists(String firstName, String lastName) {
//        String hql = "FROM com.crudapp.model.User as u WHERE u.firstName = ? and u.lastName = ?";
//        List<User> users = (List<User>) hibernateTemplate.find(hql, firstName, lastName);
        // return users.size() > 0 ? true : false;
        return false;
    }
}
