<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8" />
    <title> Java Rush CRUD App </title>
</head>
<body ng-app="myApp" class="ng-scope">
<div ng-controller="UserController as userCtrl" class="generic-container ng-scope">
    <div class="panel panel-default">
        <div class="panel-heading"><span class="lead">User Registration Form </span></div>
<div class="formcontainer">
        <form name="userForm" class="form-horizontal">

            <%--<div ng-if="userCtrl.flag != 'edit'">--%>

            <%--</div>--%>
            <%--<div ng-if="userCtrl.flag == 'edit'">--%>

            <%--</div>--%>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2 control-lable" for="input1">First Name</label>
                    <div class="col-md-7">
                        <input type="text" ng-model="userCtrl.user.userFirstName" id="input1" class="username form-control input-sm" required />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2 control-lable" for="input2">Last Name</label>
                    <div class="col-md-7">
                        <input type="text" ng-model="userCtrl.user.userLastName" id="input2" class="username form-control input-sm" required />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2 control-lable" for="input3">E-mail</label>
                    <div class="col-md-7">
                        <input type="text" ng-model="userCtrl.user.email" id="input3" class="username form-control input-sm" required />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2 control-lable" for="input4">Age</label>
                    <div class="col-md-7">
                        <input type="text" ng-model="userCtrl.user.age" id="input4" class="username form-control input-sm" required />
                    </div>
                </div>
            </div>
            <div class="row subbuttons">
                <div ng-if="userCtrl.flag != 'edit'" class="edit-buttons">
                    <input  type="submit" ng-click="userCtrl.addUser()" class="first btn btn-primary btn-sm" value="Add User"/>
                    <input type="button" ng-click="userCtrl.reset()" class="btn btn-info btn-sm" value="Reset"/>
                </div>
                <div ng-if="userCtrl.flag == 'edit'">
                    <input  type="submit" ng-click="userCtrl.updateUserDetail()" class="first btn btn-primary btn-sm" value="Update User"/>
                    <input type="button" ng-click="userCtrl.cancelUpdate()" class="btn btn-primary btn-sm" value="Cancel"/>
                </div>
            </div>
            <div class="row">
                <span ng-if="userCtrl.flag=='deleted'" class="msg-success">User successfully deleted.</span>

            </div>


    </form>

</div>

</div>

<div class="tablecontainer">
    <table class="table table-hover">
        <tr><th>ID</th><th>First Name</th><th>Last Name</th><th>E-mail</th><th>Age</th><th>Options</th></tr>
        <tr ng-repeat="row in userCtrl.users">
            <td><span ng-bind="row.userId"></span></td>
            <td><span ng-bind="row.userFirstName"></span></td>
            <td><span ng-bind="row.userLastName"></span></td>
            <td><span ng-bind="row.email"></span></td>
            <td><span ng-bind="row.age"></span></td>
            <td>
                <input type="button" ng-click="userCtrl.deleteUser(row.userId)" value="Delete" class="btn btn-danger btn-sm"/>
                <input type="button" ng-click="userCtrl.editUser(row.userId)" value="Edit" class="btn btn-success btn-sm"/>
                <span ng-if="userCtrl.flag=='updated' && row.userId==userCtrl.updatedId" class="msg-success">User successfully updated.</span>
            </td>
        </tr>
    </table>
</div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-resource.js"></script>
<script src="${pageContext.request.contextPath}/app-resources/js/app.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/app-resources/css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</body>
</html>