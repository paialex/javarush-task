CREATE DATABASE IF NOT EXISTS `test`;
USE `test`;

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(5) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `age` int(3) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO `users` (`uid`, `first_name`, `last_name`, `email`, `age`) VALUES
	(1, 'Brad', 'Pit','bpit@mail.com', 40),
	(2, 'Tom', 'Cruse','tcruse@mail.com', 36),
	(3, 'Steve', 'Jobs','sjobs@mail.com', 60);
	